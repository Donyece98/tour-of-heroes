import {Hero} from './hero'; 

export const HEROES: Hero[]=[
    {id:11, name: 'Donyece'}, 
    {id:12, name: 'Stefan'}, 
    {id:13, name: 'Sabre'}, 
    {id:14, name:'Christina'}, 
    {id:15, name:'Alaia'}, 
    {id:16, name:'Elle'}, 
    {id:17, name:'Bella'}, 
    {id:18, name:'Ryan'}, 
    {id:19, name:'Tre'}, 
    {id:20, name:'Shyla'}, 
]; 