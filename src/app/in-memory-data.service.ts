import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      {id:11, name: 'Donyece'}, 
      {id:12, name: 'Stefan'}, 
      {id:13, name: 'Sabre'}, 
      {id:14, name:'Christina'}, 
      {id:15, name:'Alaia'}, 
      {id:16, name:'Elle'}, 
      {id:17, name:'Bella'}, 
      {id:18, name:'Ryan'}, 
      {id:19, name:'Tre'}, 
      {id:20, name:'Shyla'},
    ];
    return {heroes};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}